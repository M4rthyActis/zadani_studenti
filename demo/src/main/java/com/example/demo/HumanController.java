package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.sql.*;
import java.util.List;

@SpringBootApplication
@RestController
@EnableSwagger2
public class HumanController {

	@Autowired
	private HumanService humanService;

	public static void main(String[] args)
	{
		SpringApplication.run(HumanController.class, args);
	}

	//GET REQUEST
	@GetMapping("/Humans")
	public List<Human> getHumans()
	{
		return humanService.getAllHumans();
	}

	//GET REQUEST
	@GetMapping("/Humans/{id}")
	public Human getOneHuman(@PathVariable int id)
	{
		return humanService.getHuman(id);
	}

	//POST REQUEST
	@PostMapping("/Humans/AddHuman")
	public String addHuman(@RequestBody Human human)
	{
		return humanService.addNewHuman(human);
	}

	//POST REQUEST
	@PostMapping("/Humans/AddHumans")
	public String addHuman(@RequestBody List<Human> humans)
	{
		return humanService.addNewHumans(humans);
	}

	//PUT REQUEST
	@PutMapping("/Humans/UpdateHuman")
	public void updateHuman(@RequestBody Human human)
	{
		humanService.updateHuman(human);
	}

	//DELETE REQUEST
	@DeleteMapping("/Humans/DeleteHuman/{id}")
	public void deleteHuman(@PathVariable int id)
	{
		humanService.deleteHuman(id);
	}
}