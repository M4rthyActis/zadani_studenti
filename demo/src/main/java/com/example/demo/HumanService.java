package com.example.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class HumanService {

    @Autowired
    private HumanRepository humanRepository;

    public HumanService()
    {

    }

    //Přidá jednoho Human do databáze
    public String addNewHuman(Human human)
    {
        humanRepository.save(human);
        return ("Added new human");
    }

    //Přidá více Humans do databáze
    public String addNewHumans(List<Human> humans)
    {
        humanRepository.saveAll(humans);
        return ("Added new human");
    }

    //Vrátí jednoho konkrétního Human z databáze
    public Human getHuman(int id)
    {
        Optional<Human> oh = humanRepository.findById(id);
        if(oh.isPresent())
        {
            return oh.get();
        }
        return null;
    }

    //Vrátí všechny Humans z databáze
    public List<Human> getAllHumans()
    {
        return humanRepository.findAll();
    }

    //Aktualizuje Human v databázi
    public void updateHuman(Human human)
    {
        humanRepository.save(human);
    }

    //Vymaže konkrétního Human z databáze
    public void deleteHuman(int id)
    {
        humanRepository.deleteById(id);
    }

}
