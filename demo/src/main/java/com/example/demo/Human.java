package com.example.demo;

import javax.persistence.*;

@Entity
@Table(name = "humans")
public class Human {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int age;

    private String name;

    private String birthDate;

    private boolean married;

    public Human(int age, String name, String birthDate, boolean married) {
        this.age = age;
        this.name = name;
        this.birthDate = birthDate;
        this.married = married;

    }

    public Human() {
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public boolean getMarried() {
        return married;
    }

    public void setMarried(boolean married)
    {
        this.married = married;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
